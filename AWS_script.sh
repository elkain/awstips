#!/bin/bash
#set -x
Usage() {
  echo "Usage: $0 [ -u AWSUserName ] [ -o OTP ] [ -i MFADEVICE ]" 1>&2
}

ChooseInstance() {
  DefaultCOLUMNS=${COLUMNS}
  echo ""
  PS3="On Wich instance do you want to work ?"
  COLUMNS=200
  echo "N° |     InstanceID      |    KeyName    |     Name      |            OS              | PublicIP  |  Status   |"
  select INSTANCE in $(aws ec2 describe-instances --query "Reservations[*].Instances[*].{PublicIP:PublicIpAddress,Name:Tags[?Key=='Name']|[0].Value,Status:State.Name,KeyName:KeyName,OS:PlatformDetails,InstanceID:InstanceId,SecurityGroups:SecurityGroups[0].GroupId}" --filters "Name=tag:Owner,Values='$USER_AWS'" --output table|grep '^|  i-')
  do
    COLUMNS=${DefaultCOLUMNS}
    case ${REPLY} in
      [[:digit:]]) 
        export INSTANCEID=$(echo ${INSTANCE}|awk -F \| '{print $2}'|tr -d ' ')
        export KEYNAME=$(echo ${INSTANCE}|awk -F \| '{print $3}'|tr -d ' ')
        export INSTPUBLICIP=$(echo ${INSTANCE}|awk -F \| '{print $6}'|tr -d ' ')
        export SECURITYGROUP=$(echo ${INSTANCE}|awk -F \| '{print $7}'|tr -d ' ')
        break ;;
      *)
        echo "Invalid Choice" ;;
    esac
  done
}

StartInstance() {
  INSTANCEID=${1}
  [ $(aws ec2 describe-instances --query "Reservations[*].Instances[*].{Status:State.Name}" --instance-ids ${INSTANCEID} --output text) != "running" ] && aws ec2 start-instances --instance-ids ${INSTANCEID} || (echo "Instance already running" ; ChooseExit)
  echo -n "Starting in progress..."
  until [ $(aws ec2 describe-instances --query "Reservations[*].Instances[*].{Status:State.Name}" --instance-ids ${INSTANCEID} --output text) == "running" ]
  do
    echo -n "..."
    sleep 5s
  done
  echo -e "\nInstance started\n"
}

StopInstance() {
  INSTANCEID=${1}
  [ $(aws ec2 describe-instances --query "Reservations[*].Instances[*].{Status:State.Name}" --instance-ids ${INSTANCEID} --output text) != "stopped" ] && aws ec2 stop-instances --instance-ids ${INSTANCEID} || (echo "Instance already stopped" ; ChooseExit)
  echo -n "Stopping in progress..."
  until [ $(aws ec2 describe-instances --query "Reservations[*].Instances[*].{Status:State.Name}" --instance-ids ${INSTANCEID} --output text) == "stopped" ]
  do
    echo -n "..."
    sleep 5s
  done
  echo -e "\nInstance stopped\n"
}

SSHInstance() {
  INSTANCEID=${1}
  KEYNAME=${2}
  INSTPUBLICIP=$(aws ec2 describe-instances --query "Reservations[*].Instances[*].{PublicIP:PublicIpAddress}" --instance-ids ${INSTANCEID})
  [  ${INSTPUBLICIP} != "None" ] && ssh ec2-user@${INSTPUBLICIP} -i ~/${KEYNAME}.pem || echo "Instance has no Public IP (perhaps not started)."
}

ChooseSGAction() {
  COLUMNS=200
  echo ""
  PS3="What do you want to do ?"
  select CHOICE in "Delete a rule" "Add a rule" "No other actions"
  do
    COLUMNS=${DefaultCOLUMNS}
    case ${REPLY} in
      1)
        #Delete a rule
        RemovePublicIP ${SECURITYGROUP}
        ChooseSGAction
        ;;
      2)
        #Add a rule
        AddPublicIP ${SECURITYGROUP}
        ChooseSGAction
        ;;
      3)
        echo "No other action"
        ChooseExit
        ;;
      *)
        echo "Invalid Choice" ;;
    esac
  done
}

RemovePublicIP() {
  SECURITYGROUP=${1}
  # List all rules for this SG
  aws ec2 describe-security-groups --group-ids ${SECURITYGROUP} --output table --query "SecurityGroups[0].{Name:GroupName,Description:Description,GroupId:GroupId,IpPermissions:IpPermissions}"
  # Read the port and the IP to remove
  read -p "What is the Input IP rule to remove (with the netmask) ?" INIP
  read -p "On wich port (only tcp) ?" PORT
  # Delete rule related to the previous publicIP
  aws ec2 revoke-security-group-ingress --group-id ${SECURITYGROUP} --protocol tcp --port ${PORT} --cidr ${INIP}
}

AddPublicIP() {
  SECURITYGROUP=${1}
  echo "What is the INPUT ip (with netmask) ?"
  read -p "(For your publicIP use \"MYIP\") " INIP
  read -p "On which port (only tcp) ?" PORT
  read -p "What is the description ?" DESCRIPTION
  # Create the new rule for the current Public IP
  if [ ${INIP} == "MYIP" ]
  then
    INIP=$(curl -s ifconfig.me)
  fi
  aws ec2 authorize-security-group-ingress --group-id ${SECURITYGROUP} --ip-permissions '[{"IpProtocol": "tcp", "FromPort": 22, "ToPort": 22, "IpRanges": [{"CidrIp": "'${INIP}'/32", "Description": "'${DESCRIPTION}'"}]}]'
}

ChooseAction() {
  COLUMNS=200
  echo ""
  PS3="What do you want to do ?"
  select CHOICE in "Start the instance" "Stop the instance" "SSH to the instance" "Start and SSH this instance" "Modify the securitygroup rule"
  do
    COLUMNS=${DefaultCOLUMNS}
    case ${REPLY} in
      1) 
        #Start the instance
        StartInstance ${INSTANCEID}
        ChooseExit
        ;;
      2)
        #Stop the instance
        StopInstance ${INSTANCEID}
        ChooseExit
        ;;
      3)
        #SSH to the instance
        SSHInstance ${INSTANCEID} ${KEYNAME}
        ChooseExit
        ;;
      4)
        # Start the instance and SSH to it.
        #Start the instance
        StartInstance ${INSTANCEID}
        sleep 5s
        #SSH to the instance
        SSHInstance ${INSTANCEID} ${KEYNAME}
        ChooseExit
        ;;
      5)
        #Allow my public IP in security group
        ChooseSGAction
        ;;
      *)
        echo "Invalid Choice" ;;
    esac
  done
}

ChooseExit(){
  COLUMNS=10
  echo ""
  PS3="Do you want to do another action ?"
  select CHOICE in "Yes, on the same instance" "Yes, on another instance" "No"
  do
    COLUMNS=${DefaultCOLUMNS}
    case ${REPLY} in 
      1)
        #Yes same instance
        ChooseAction
        break;;
      2)
        #Yes another instance
        ChooseInstance
        ChooseAction
        break;;
      3)
        #No
        exit;;
      *)
        echo "Invalid Choice";;
    esac
  done
}

while getopts ":u:o:i:" opt
do
  case $opt in
    u)
      USER_AWS=${OPTARG}
      ;;
    o)
      OTP=${OPTARG}
      ;;
    i)
      MFADEVICE=${OPTARG}
  esac
done

#Set as variable 'USER' the login of the user
if [ -z ${USER_AWS} ]
then
  read -p "AWS User : " USER_AWS
fi

# Set as variable 'OTP' the OTP code for MFA
if [ -z ${OTP} ]
then
  read -p "OTP Code for AWS : " OTP
fi

# Set as variable 'MFADEVICE' the MFA device name
if [ -z ${MFADEVICE} ]
then
  read -p "MFA device name for AWS : " MFADEVICE
fi

# Get AWS authentication variable
eval AWS="($(aws sts get-session-token --serial-number  ${MFADEVICE}/${USER_AWS} --token-code ${OTP}))"

# Set variables for authentication
export AWS_ACCESS_KEY_ID=${AWS[1]}
export AWS_SECRET_ACCESS_KEY=${AWS[3]}
export AWS_SESSION_TOKEN=${AWS[4]}

IFS=$'\n'

ChooseInstance
ChooseAction
ChooseExit
