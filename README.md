# AWSTips
Some scripts and tips I used to manage and work on AWS.

## AWS_script.sh:
This script permite simples actions on AWS instances.
It show only the instance with a tag "Owner" equal to the AWSUserName.
It work on every Linux/Unix (bash required due to globbing), VM, WSL, docker, ...  
Only side package requirement is to use the auto-public IP addiction to the Security group : curl

#### Usage:
* Copy the script on your host.
* Create an Access key from the AWS interface.
* Launch the command `aws configure` and reply to the question with this key.
/!\ The `Default output format` must be `text`.
* Copy the MFA device to the "/" (ex : arn:aws:iam::245096513645:mfa ). It is the `-i` option on command line of the script.
* Launch the command
  `./AWS_script.sh [ -u AWSUserName ] [ -o OTP ] [ -i MFADevice ]`
